package me.voidcrafted.barcodeweb

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.JsonRequest
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*
import com.google.zxing.integration.android.IntentIntegrator
import org.json.JSONObject


class Main : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        scan.setOnClickListener {
            val integrator = IntentIntegrator(this)
            integrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES)
            integrator.setPrompt("Scan Code")
            integrator.setCameraId(0)
            integrator.setBeepEnabled(false)
            integrator.captureActivity = CaptureActivityPortrait::class.java
            integrator.setBarcodeImageEnabled(false)
            integrator.initiateScan()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val intentResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)
                ?: return
        if (intentResult.contents == null) {
            Toast.makeText(this, "Cancelled!", Toast.LENGTH_LONG).show()
        } else {
            Toast.makeText(this, "Scanned: " + intentResult.contents, Toast.LENGTH_LONG).show()
            val queue = Volley.newRequestQueue(this)
            val url = "https://scan.bunbuns.club/new"
            val jsonObject = JSONObject()
            jsonObject.put("barcode", intentResult.contents)
            val jsonObjectRequest = JsonObjectRequest(
                    Request.Method.POST,
                    url,
                    jsonObject,
                    Response.Listener { },
                    Response.ErrorListener { error ->
                        // TODO: Handle error
                    }
            )
            queue.add(jsonObjectRequest)
        }
    }
}
