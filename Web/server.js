const express = require("express");
const app = express();
const bodyParser = require("body-parser");
var http = require('http').Server(app);
var io = require('socket.io')(http);

app.use(bodyParser.json())
app.use(express.static("static"))
app.post("/new", (req, res) => {
    io.emit("barcode", req.body.barcode)
    res.end("{}")
})

io.on('connection', function(socket){
    console.log('a user connected');
});

http.listen(3000)